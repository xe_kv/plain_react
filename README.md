# Sample project

---

### Project structure: 

1. /react-playground - project with rollup which allows you to bundle your components to ES-5 minified version with selected presets.
2. /site - sample html page with builded component inserted.


### How to launch?
1. Install npm modules for both directories.
2. cd ./react-playground
3. Edit component in ./react-playground/src/Components/Sample
4. npm run build
5. Minified version of build created in ./react-playground/dist
6. Copy created file to /site
7. Inside ./site directory npm start
7. Test if it work D:


### TODO
1. ENHANCE CONFIG TO SUPPORT MULTIPLE BUILDS (RIGHT NOW 1 COMPONENT BUILD HARDCODED)
