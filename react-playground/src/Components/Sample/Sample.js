import React from 'react';

class Sample extends React.Component {
  constructor(props) {
    super(props);
    this.state = { number: 0 };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({ number: this.state.number + 1 });
  }

  render() {
    const { number } = this.state;
    return (
      <div>
        <span>Hello, I'm react</span>
        <button onClick={this.handleClick}>Click me pls</button>
        <span>{number}</span>
      </div>
    )
  }
}

export default Sample;
