import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';
import externalGlobals from 'rollup-plugin-external-globals';
import { uglify } from 'rollup-plugin-uglify';


export default {
  input: 'src/Components/Sample/index.js',
  moduleName: 'Sample',
  output: {
    file: 'dist/component.js',
    format: 'iife'
  },
  external: [
    'react',
    'react-proptypes'
  ],
  plugins: [
    resolve(),
    uglify(),
    babel({
      exclude: 'node_modules/**',
    }),
    externalGlobals({
      react: 'React',
    })
  ]
};